<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/8609eee3c1.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/merchant-item.css">
    <?php $this->load->view('sidebar-navbar');?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div class="row align-item-center mt-4">
                <div class="col-lg-4">
                    <h3>Merchant Items</h3>
                </div>
                <form class="tittle-box-form1 col-lg-8" role="search">
                    <div class="row justify-content-center">
                        <div class="col-7 col-lg-3">
                            <input class="form-control input-edit" type="search" placeholder="From Date" aria-label="Search">
                        </div>
                        <div class="col-7 col-lg-3">
                            <input class="form-control input-edit" type="search" placeholder="To Date" aria-label="Search">
                        </div>
                        <div class="col-7 col-lg-3">
                            <input class="form-control input-edit" type="search" placeholder="Status" aria-label="Search">
                        </div>
                    </div>
                </form>
            </div>
        </div>   
        <div class="container"> 
            <div class="card">
                <div class="row p-4">
                    <form class="tittle-box-form2 col-lg-10" role="search">
                        <div class="row">
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="Search" aria-label="Search">                                             
                            </div>
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="From Date" aria-label="Search">                                            
                            </div>
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="To Date" aria-label="Search">                                            
                            </div>
                            <div class="col-6 col-lg-2">
                                <select class="form-select input-edit" id="status" name="status" aria-label="Default select example">
                                    <option selected>Status</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>                                            
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn p-0" name="cancel" id="cancel123"><i class="material-icons check-icon">&#xe5d5;</i></button>
                                <button type="button" class="btn p-0" name="cancel" id="cancel167"><i class="material-icons check-icon">&#xe8b6;</i></button> 
                            </div>
                        </div>
                    </form>
                    <div class="col-lg-2">
                            <a href="<?php echo base_url();?>add_product" class="btn add-btn">
                                Add
                            </a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-borderless text-left">
                        <thead>
                            <th>Merchant</th>
                            <th>Branch</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Created Date</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($productlist as $list) {  
                                ?>
                            <tr>
                                <td><?php echo $list->Product_name?></td>
                                <td><?php echo $list->Product_branch?></td>
                                <td><?php echo $list->Product_category?></td>
                                <td><?php echo $list->Product_sub_category?></td>
                                <td><img class="me-2" src="<?php echo base_url();?>assets/images/noodles.png"><?php echo $list->Product_nam?></td>
                                <td><?php echo $list->Product_price?></td>
                                <td>22/08/23</td>
                                <td class="text-center <?php echo ($list->Product_status == 'Active')?"tbl-active":""?>"><?php echo $list->Product_status?></td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            <button>...</button>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="#"><i class="material-icons text-center"></i>View</a></li>
                                            <li><a class="dropdown-item" href="#"><i class="material-icons text-center"></i>delete</a></li>
                                            <li><a class="dropdown-item" href="#"><i class="material-icons text-center"></i>Edit</a></li>
                                        </ul>
                                    </div> 
                                </td>
                            </tr>
                        <?php }  
                        ?> 
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-end mx-4">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>                
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
</body>