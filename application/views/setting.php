<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/setting.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div class="row my-3">
                <h3>Settings</h3>
            </div>
            <div class="card">
                <div class="row mt-2 ms-3">
                    <ul class="nav nav-underline" id="myTab" role="tablist">
                        <div class="row w-100 justify-content-between">
                            <li class="nav-item col-6 col-sm-6 col-md-6 col-lg" role="presentation">
                                <button class="nav-link text-center active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">General</button>
                            </li>
                            <li class="nav-item col-6 col-sm-6 col-md-6 col-lg" role="presentation">
                                <button class="nav-link text-center" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Roles</button>
                            </li>
                            <li class="nav-item col-6 col-sm-6 col-md-6 col-lg" role="presentation">
                                <button class="nav-link text-center" id="profile-tab1" data-bs-toggle="tab" data-bs-target="#profile-tab-pane1" type="button" role="tab" aria-controls="profile-tab-pane1" aria-selected="false">Country</button>
                            </li>
                            <li class="nav-item col-6 col-sm-6 col-md-6 col-lg" role="presentation">
                                <button class="nav-link text-center" id="profile-tab2" data-bs-toggle="tab" data-bs-target="#profile-tab-pane2" type="button" role="tab" aria-controls="profile-tab-pane2" aria-selected="false">State</button>
                            </li>
                            <li class="nav-item col-6 col-sm-6 col-md-6 col-lg" role="presentation">
                                <button class="nav-link text-center" id="profile-tab3" data-bs-toggle="tab" data-bs-target="#profile-tab-pane3" type="button" role="tab" aria-controls="profile-tab-pane3" aria-selected="false">City</button>
                            </li>
                            <li class="nav-item col-6 col-sm-6 col-md-6 col-lg" role="presentation">
                                <button class="nav-link text-center text-nowrap" id="profile-tab4" data-bs-toggle="tab" data-bs-target="#profile-tab-pane4" type="button" role="tab" aria-controls="profile-tab-pane4" aria-selected="false">Category Type</button>
                            </li>
                            <li class="nav-item col-6 col-sm-6 col-md-6 col-lg" role="presentation">
                                <button class="nav-link text-center text-nowrap" id="profile-tab5" data-bs-toggle="tab" data-bs-target="#profile-tab-pane5" type="button" role="tab" aria-controls="profile-tab-pane5" aria-selected="false">Notification</button>
                            </li>
                        </div>
                    </ul>
                    <hr class="tabs-line">
                </div>
                <div class="tab-content ms-3 mt-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                        <div class="card card-3 p-4 mb-4">
                            <div class="row">
                                <p class="bold-text">Personal Details</p>
                            </div>
                            <form action="" class="row forms1">
                                <?php
                                    foreach ($profile as $obj)  
                                    {  
                                        ?>
                                        <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="form-label" for="fullname">Full Name</label>
                                                <input type="text" class="form-control input-edit-form" name="fullname" id="fullname"
                                                    placeholder="<?php  echo $obj['fullname'] ?>" readonly>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="form-label" for="contact">Contact Address</label>
                                                <input type="text" class="form-control input-edit-form" name="contact" id="contact"
                                                    placeholder="<?php echo $obj['contactnumber'] ?>" readonly>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="form-label" for="email">Email Address</label>
                                                <input type="text" class="form-control input-edit-form" name="email" id="email"
                                                    placeholder="<?php echo $obj['email'] ?>" readonly>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="form-label" for="address">Address</label>
                                                <input type="text" class="form-control input-edit-form" name="address" id="address"
                                                    placeholder="<?php echo $obj['address'] ?>" readonly>
                                            </div>
                                        </div>
                                    </div>  
                                    <?php }  
                                    ?>  
                                <div class="col-lg-2">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <img src="<?php echo base_url();?>assets/images/customerfull.png" class="img-fluid">
                                        </div>
                                        <div class="col text-center mt-3">
                                            <span class="orange-txt">Change Profile Image<span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card card-3 p-4">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="row height-edit align-items-center">
                                        <p class="bold-text">Delete Account</p>
                                    </div>                                    
                                </div>
                                <div class="col-lg-7">
                                    <div class="row">
                                        <p class="bold-text col-12">Delete your account and all of your source data. This is irreversible.</p>
                                        <a class="btn lorange-btn col-4 mt-1 ms-2" href="#">Delete Account</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                        <div class="card card-2 mx-auto pt-3 px-5" id="role">
                            <div class="row justify-content-between">
                                <h3 class="col-8 col-md-6 col-lg-5 text-nowrap">Adminstrators account</h3>
                                <button type="button" id="role-sub" href="#" class="btn orange-btn col-3 col-md-2 col-lg-1">Add</a>
                            </div>
                            <div class="row">
                                <p class="color-p">Find all of your company's administrator account and their associate roles. </p>
                            </div>
                            <div class="row mt-1">
                                <table class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Role</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Super Admin</td>
                                            <td class="tbl-active">Active</td>
                                            <td><i class="fas fa-pencil-alt edit-icon me-2 mb-2"></i>
                                            <i class="material-icons delete-icon">&#xe872;</i></td>
                                        </tr>
                                        <tr>    
                                            <th scope="row">2</th>
                                            <td>Super Admin</td>
                                            <td class="tbl-active">Active</td>
                                            <td><i class="fas fa-pencil-alt edit-icon me-2"></i>
                                            <i class="material-icons delete-icon">&#xe872;</i></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>Super Admin</td>
                                            <td class="tbl-active">Active</td>
                                            <td><i class="fas fa-pencil-alt edit-icon me-2"></i>
                                            <i class="material-icons delete-icon">&#xe872;</i></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="role1" class="d-none">
                            <div class="card-2 p-4">
                                <form class="row align-items-center">
                                    <p class="orange-txt col-6 col-md-1 col-lg-1 mb-0">Role</p>
                                    <div class="col-6 col-md-4 col-lg-4">
                                    <input type="text" class="form-control input-edit-form" name="contact" id="contact"
                                                placeholder="Type here">
                                    </div>
                                    <p class="orange-txt col-6 col-md-1 col-lg-1 mb-0">Status</p>
                                    <div class="col-6 col-md-4 col-lg-4">
                                        <select class="form-select input-edit-form" id="status1" name="status1" aria-label="Default select example">
                                                <option selected>Status</option>
                                                <option value="Active">Active</option>
                                                <option value="Inactive">Inactive</option>
                                        </select>
                                    </div>                                   
                                </form>
                                <div class="row mt-1">
                                    <table class="table table-borderless mt-3 check-table">
                                        <thead>
                                            <tr>
                                                <th>Module</th>
                                                <th>Add</th>
                                                <th>Edit</th>
                                                <th>View</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Dashboard</td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Merchant</td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Merchant & Items</td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Category</td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Sub-Category</td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Orders</td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Customer</td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input manual-check p-2" type="checkbox" value="" id="flexCheckDefault">
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row justify-content-center column-gap-2 mt-3">
                                    <a href="#" class="btn orange-border col-3 col-lg-2" id="rolecancel">Discard</a>
                                    <a href="#" class="btn orange-btn col-3 col-lg-2">Submit</a>
                                </div>                               
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile-tab-pane1" role="tabpanel" aria-labelledby="profile-tab1" tabindex="0">
                        <div class="card card-2 mx-auto pt-3 px-5" id="country">
                            <div class="row justify-content-between">
                                <h3 class="col-8 col-md-6 col-lg-5 text-nowrap">Country Setting</h3>
                                <button type="button" href="#" class="btn orange-btn col-3 col-md-2 col-lg-1" id="countryadd">Add</button>
                            </div>
                            <div class="row">
                                <p class="color-p">Find all of your company's administrator account and their associate roles. </p>
                            </div>
                            <div class="row py-3">
                                <form class="tittle-box-form2 col-lg-10">
                                    <div class="row">
                                        <div class="col-6 col-lg-4">
                                            <select class="form-select input-edit" id="status1" name="status1" aria-label="Default select example">
                                                <option selected>Country</option>
                                                <?php
                                                    foreach ($infos as $row) {  
                                                        ?>
                                                        <option value="<?php echo $row->country_name;?>"><?php echo $row->country_name;?></option>
                                                    <?php }  
                                                    ?> 
                                            </select>                                            
                                        </div>
                                        <div class="col-6 col-lg-4">
                                            <select class="form-select input-edit" id="status" name="status" aria-label="Default select example">
                                                <option selected>Status</option>
                                                <option value="Active">Active</option>
                                                <option value="Inactive">Two</option>
                                            </select>                                            
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn p-0" name="cancel" id="cancel123"><i class="material-icons check-icon">&#xe5d5;</i></button>
                                            <button type="button" class="btn p-0" name="cancel" id="cancel167"><i class="material-icons check-icon">&#xe8b6;</i></button> 
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="row mt-1">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Country</th>
                                            <th scope="col">Action</th>
                                            <th scope="col">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=1;  
                                        foreach ($infos as $row)  
                                        {  
                                            ?><tr>
                                                <td><?php echo $i;?></td>
                                                <td><?php echo $row->country_name;?></td>
                                                <td><i class="fas fa-pencil-alt edit-icon me-2"></i>
                                                <i class="material-icons delete-icon">&#xe872;</i></td>
                                                <td class="<?php echo ($row->status=='Active')?'tbl-active':''?>"><?php echo $row->status;?></td>
                                            </tr>  
                                        <?php $i++; }  
                                        ?>  
                                        <!-- <tr>
                                            <th scope="row">1</th>
                                            <td>Super Admin</td>
                                            <td><i class="fas fa-pencil-alt edit-icon me-2"></i>
                                            <i class="material-icons delete-icon">&#xe872;</i></td>
                                            <td class="tbl-active">Active</td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card card-2 p-4 d-none" id="country1">
                            <h3>Add Country</h3>
                            <form action="Settings/country_type" class="row" method="post">
                                <div class="col-lg-4">
                                    <label class="form-label" for="countryname">Country Name</label>
                                    <div>
                                        <input type="text" class="form-control input-edit-form" name="country" id="countryname" placeholder="Type here">
                                    </div>  
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label" for="status">Status</label>
                                    <select class="form-select input-edit-form" id="status" name="status" aria-label="Default select example">
                                            <option selected>Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                                <div class="row justify-content-center column-gap-2 mt-4">
                                    <a href="#" class="btn orange-border col-1" id="countrycancel">Discard</a>
                                    <!-- <a href="#" class="btn orange-btn col-1">Submit</a> -->
                                    <button type="submit" class="btn orange-btn col-1" id="formsubmit" name="countrysubmit" value="Submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile-tab-pane2" role="tabpanel" aria-labelledby="profile-tab2" tabindex="0">
                        <div class="card card-2 mx-auto pt-3 px-5" id="state">
                            <div class="row justify-content-between">
                                <h3 class="col-8 col-md-6 col-lg-5 text-nowrap">Country Setting</h3>
                                <button type="button" href="#" class="btn orange-btn col-3 col-md-2 col-lg-1" id="stateadd">Add</button>
                            </div>
                            <div class="row">
                                <p class="color-p">Find all of your company's administrator account and their associate roles. </p>
                            </div>
                            <div class="row py-3">
                                <form class="tittle-box-form2 col-lg-10">
                                    <div class="row">
                                        <div class="col-6 col-lg-4">
                                            <select class="form-select input-edit" id="status1" name="status1" aria-label="Default select example">
                                                <option selected>Country</option>
                                                <?php
                                                    foreach ($states as $row) {  
                                                        ?>
                                                        <option value="<?php echo $row->con_name;?>"><?php echo $row->con_name;?></option>
                                                    <?php }  
                                                    ?>
                                            </select>                                            
                                        </div>
                                        <div class="col-6 col-lg-3">
                                            <select class="form-select input-edit" id="status" name="status" aria-label="Default select example">
                                                <option selected>State</option>
                                                <?php
                                                    foreach ($states as $row) {  
                                                        ?>
                                                        <option value="<?php echo $row->state_name;?>"><?php echo $row->state_name;?></option>
                                                    <?php }  
                                                    ?>
                                            </select>                                            
                                        </div>
                                        <div class="col-6 col-lg-3">
                                            <select class="form-select input-edit" id="status" name="status" aria-label="Default select example">
                                                <option selected>Status</option>
                                                <option value="Active">Active</option>
                                                <option value="Inactive">Inactive</option>
                                            </select>                                            
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn p-0" name="cancel" id="cancel123"><i class="material-icons check-icon">&#xe5d5;</i></button>
                                            <button type="button" class="btn p-0" name="cancel" id="cancel167"><i class="material-icons check-icon">&#xe8b6;</i></button> 
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="row mt-1">
                                <table class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Country</th>
                                            <th scope="col">State</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=1;  
                                        foreach ($states as $row)  
                                        {  
                                            ?><tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $row->con_name;?></td>
                                            <td><?php echo $row->state_name;?></td>
                                            <td class="<?php echo($row->state_status == "Active")?"tbl-active":" "?>"><?php echo $row->state_status;?></td>
                                            <td><i class="fas fa-pencil-alt edit-icon me-2"></i>
                                            <i class="material-icons delete-icon">&#xe872;</i></td>    
                                            </tr>  
                                        <?php $i++; }  
                                        ?>
                                        <!-- <tr>
                                            <th scope="row">1</th>
                                            <td>India</td>
                                            <td>Tamil Nadu</td>
                                            <td class="tbl-active">Active</td>
                                            <td><i class="fas fa-pencil-alt edit-icon me-2"></i>
                                            <i class="material-icons delete-icon">&#xe872;</i></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card card-2 p-4 d-none" id="state1">
                            <h3>Add State</h3>
                            <form action="Settings/state_type" class="row" method="post">
                                <div class="col-lg-4">
                                    <label class="form-label" for="countryname">Country Name</label>
                                    <select class="form-select input-edit-form" id="countryname" name="countryname" aria-label="Default select example">
                                            <option selected>Country name</option>
                                            <?php
                                                foreach ($infos as $row) {  
                                                    ?>
                                                    <option value="<?php echo $row->country_name;?>"><?php echo $row->country_name;?></option>
                                                <?php }  
                                                ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label" for="statename">State Name</label>
                                    <div>
                                        <input type="text" class="form-control input-edit-form" name="statename" id="statename" placeholder="Type here">
                                    </div> 
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label" for="status">Status</label>
                                    <select class="form-select input-edit-form" id="status" name="statestatus" aria-label="Default select example">
                                            <option selected>Status</option>
                                            <option value="Active">active</option>
                                            <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                                <div class="row justify-content-center column-gap-2 mt-4">
                                    <a href="#" class="btn orange-border col-1" id="statecancel">Discard</a>
                                    <!-- <a href="#" class="btn orange-btn col-1">Submit</a> -->
                                    <button type="submit" class="btn orange-btn col-1" id="formsubmit" name="statesubmit" value="Submit">Submit</button>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile-tab-pane3" role="tabpanel" aria-labelledby="profile-tab3" tabindex="0">
                        <div class="card card-2 mx-auto pt-3 px-5" id="city">
                            <div class="row justify-content-between">
                                <h3 class="col-8 col-md-6 col-lg-5 text-nowrap">Country Setting</h3>
                                <button href="#" class="btn orange-btn col-3 col-md-2 col-lg-1" id="cityadd">Add</button>
                            </div>
                            <div class="row">
                                <p class="color-p">Find all of your company's administrator account and their associate roles. </p>
                            </div>
                            <div class="row py-3">
                                <form class="tittle-box-form2 col-lg-10">
                                    <div class="row">
                                        <div class="col-6 col-lg-4">
                                            <select class="form-select input-edit" id="status1" name="status1" aria-label="Default select example">
                                                <option selected>Country</option>
                                                <?php  
                                                    foreach ($citys as $row)  
                                                    {  
                                                        ?>
                                                        <option value="<?php echo $row->city_country;?>"><?php echo $row->city_country;?></option> 
                                                    <?php }  
                                                    ?>
                                            </select>                                            
                                        </div>
                                        <div class="col-6 col-lg-2">
                                            <select class="form-select input-edit" id="status" name="status" aria-label="Default select example">
                                                <option selected>State</option>
                                                <?php  
                                                    foreach ($citys as $row)  
                                                    {  
                                                        ?>
                                                        <option value="<?php echo $row->city_state;?>"><?php echo $row->city_state;?></option> 
                                                    <?php }  
                                                    ?>
                                            </select>                                            
                                        </div>
                                        <div class="col-6 col-lg-2">
                                            <select class="form-select input-edit" id="status" name="status" aria-label="Default select example">
                                                <option selected>City</option>
                                                <?php  
                                                    foreach ($citys as $row)  
                                                    {  
                                                        ?>
                                                        <option value="<?php echo $row->city_name;?>"><?php echo $row->city_name;?></option> 
                                                    <?php }  
                                                    ?>
                                            </select>                                            
                                        </div>
                                        <div class="col-6 col-lg-2">
                                            <select class="form-select input-edit" id="status" name="status" aria-label="Default select example">
                                                <option selected>Status</option>
                                                <option value="Active">Active</option>
                                                <option value="Inactive">Inactive</option>
                                            </select>                                            
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn p-0" name="cancel" id="cancel123"><i class="material-icons check-icon">&#xe5d5;</i></button>
                                            <button type="button" class="btn p-0" name="cancel" id="cancel167"><i class="material-icons check-icon">&#xe8b6;</i></button> 
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="row mt-1">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Country</th>
                                                <th scope="col">State</th>
                                                <th scope="col">City</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $i=1;  
                                            foreach ($citys as $row)  
                                            {  
                                                ?><tr>
                                                <td><?php echo $i;?></td>
                                                <td><?php echo $row->city_country;?></td>
                                                <td><?php echo $row->city_state;?></td>
                                                <td><?php echo $row->city_name;?></td>
                                                <?php echo ($row->city_status == "Active")? "<td class='tbl-active'>". $row->city_status."</td>": "<td>" .$row->city_status."</td>"?>
                                                <td><i class="fas fa-pencil-alt edit-icon me-2"></i>
                                                <i class="material-icons delete-icon">&#xe872;</i></td>    
                                                </tr>  
                                            <?php $i++; }  
                                            ?>
                                            <!-- <tr>
                                                <th scope="row">1</th>
                                                <td>India</td>
                                                <td>Tamil Nadu</td>
                                                <td>Chennai</td>
                                                <td class="tbl-active">Active</td>
                                                <td><i class="fas fa-pencil-alt edit-icon me-2"></i>
                                                <i class="material-icons delete-icon">&#xe872;</i></td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card card-2 p-4 d-none" id="city1">
                            <h3>Add City</h3>
                            <form action="Settings/city_type" class="row" method="post">
                                <div class="col-lg-4">
                                    <label class="form-label" for="citycountry">Country Name</label>
                                    <select class="form-select input-edit-form" id="countryname" name="citycountry" aria-label="Default select example">
                                            <option selected>Country name</option>
                                            <?php
                                                foreach ($infos as $row) {  
                                                    ?>
                                                    <option value="<?php echo $row->country_name;?>"><?php echo $row->country_name;?></option>
                                                <?php }  
                                                ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label" for="citystate">State Name</label>
                                    <select class="form-select input-edit-form" id="statename" name="citystate" aria-label="Default select example">
                                            <option selected>State name</option>
                                            <?php
                                                foreach ($states as $row) {  
                                                    ?>
                                                    <option value="<?php echo $row->state_name;?>"><?php echo $row->state_name;?></option>
                                                <?php }  
                                                ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label" for="cityname">City Name</label>
                                    <div>
                                        <input type="text" class="form-control input-edit-form" name="cityname" id="cityname" placeholder="Type here">
                                    </div> 
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label" for="citystatus">Status</label>
                                    <select class="form-select input-edit-form" id="status" name="citystatus" aria-label="Default select example">
                                            <option selected>Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                                <div class="row justify-content-center column-gap-2 mt-4">
                                    <a href="#" class="btn orange-border col-1" id="citycancel">Discard</a>
                                    <!-- <a href="#" class="btn orange-btn col-1">Submit</a> -->
                                    <button type="submit" class="btn orange-btn col-1" id="formsubmit" name="citysubmit" value="Submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile-tab-pane4" role="tabpanel" aria-labelledby="profile-tab4" tabindex="0">
                        <div class="card card-2 mx-auto pt-3 px-5" id="cuisine">
                            <div class="row justify-content-between">
                                <h3 class="col-8 col-md-6 col-lg-5 text-nowrap">Cuisine Setting</h3>
                                <button type="button" href="#" class="btn orange-btn col-3 col-md-2 col-lg-1" id="cuisineadd">Add</button>
                            </div>
                            <div class="row">
                                <p class="color-p">Find all of your company's administrator account and their associate roles. </p>
                            </div>
                            <div class="row py-3">
                                <form class="tittle-box-form2 col-lg-10">
                                    <div class="row">
                                        <div class="col-6 col-lg-4">
                                            <select class="form-select input-edit" id="status1" name="status1" aria-label="Default select example">
                                                <option selected>Cuisine</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>                                            
                                        </div>
                                        <div class="col-6 col-lg-4">
                                            <select class="form-select input-edit" id="status" name="status" aria-label="Default select example">
                                                <option selected>Status</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>                                            
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn p-0" name="cancel" id="cancel123"><i class="material-icons check-icon">&#xe5d5;</i></button>
                                            <button type="button" class="btn p-0" name="cancel" id="cancel167"><i class="material-icons check-icon">&#xe8b6;</i></button> 
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="row mt-1">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Cuisine</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $i=1;  
                                            foreach ($cuisine as $row)  
                                            {  
                                                ?><tr>
                                                <td><?php echo $i;?></td>
                                                <td><?php echo $row->cuisine_name;?></td>
                                                <?php echo ($row->cuisine_status == "Active")? "<td class='tbl-active'>". $row->cuisine_status."</td>": "<td>" .$row->cuisine_status."</td>"?>
                                                <td><i class="fas fa-pencil-alt edit-icon me-2"></i>
                                                <i class="material-icons delete-icon">&#xe872;</i></td>    
                                                </tr>   
                                            <?php $i++; }  
                                            ?>
                                            <!-- <tr>
                                                <th scope="row">1</th>
                                                <td>South-Indian</td>
                                                <td class="tbl-active">Active</td>
                                                <td><i class="fas fa-pencil-alt edit-icon me-2"></i>
                                                <i class="material-icons delete-icon">&#xe872;</i></td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card card-2 p-4 d-none" id="cuisine1">
                            <h3>Add Cuisine</h3>
                            <form action=" " class="row" method="post">
                                <div class="col-lg-4">
                                    <label class="form-label" for="cuisinename">Cuisine Name</label>
                                    <div>
                                        <input type="text" class="form-control input-edit-form" name="cuisinename" id="cuisinename" placeholder="Type here">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label" for="cuisinestatus">Status</label>
                                    <select class="form-select input-edit-form" id="cuisinestatus" name="cuisinestatus" aria-label="Default select example">
                                            <option selected>Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                                <div class="row justify-content-center column-gap-2 mt-4">
                                    <a href="#" class="btn orange-border col-1" id="cuisinecancel">Discard</a>
                                    <button type="submit" class="btn orange-btn col-1" id="formsubmit" name="cuisinesubmit" value="Submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile-tab-pane5" role="tabpanel" aria-labelledby="profile-tab5" tabindex="0">
                        <div class="card card-3 p-4 mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <div class="row align-items-center height-edit">
                                        <p class="bold-text col">General</p>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="row">
                                        <p class="bold-text">System Alerts</p>
                                        <ul class="ms-4">
                                            <li>Receive important system-wide alerts, such as maintenance updates or critical issues.</li>
                                            <li>Get notified about any important announcements or updates related to the software or platform.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-check form-switch d-flex justify-content-center height-edit align-items-center">
                                        <input class="form-check-input switch-edit" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-3 p-4 mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <div class="row align-items-center height-edit">
                                        <p class="bold-text col">Merchant</p>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="row">
                                        <p class="bold-text">Merchant Notifications:</p>
                                        <ul class="ms-4">
                                            <li>Receive notifications when a new merchant registers on the platform.</li>
                                            <li>Receive updates on changes to merchant account information or settings.</li>
                                        </ul>
                                    </div>
                                    <div class="row">
                                        <p class="bold-text">Admin Portal Notifications:</p>
                                        <ul class="ms-4">
                                            <li>Get notified when a new admin user is added to the system..</li>
                                            <li>Receive notifications about changes in admin user permissions or access levels.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="row height-edit1">
                                        <div class="form-check form-switch d-flex justify-content-center  align-items-center">
                                            <input class="form-check-input switch-edit" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked>
                                        </div>
                                    </div>
                                    <div class="row height-edit1">
                                        <div class="form-check form-switch d-flex justify-content-center  align-items-center">
                                            <input class="form-check-input switch-edit" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-3 p-4 mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <div class="row align-items-center height-edit">
                                        <p class="bold-text col">Customer(Web & App)</p>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="row">
                                        <p class="bold-text">Customer App Notifications:</p>
                                        <ul class="ms-4">
                                            <li>Receive notifications when customers place orders through the app.</li>
                                            <li>Get notified about order status updates, such as order confirmed, in-progress, or delivered.</li>
                                            <li>Receive notifications about promotional offers or discounts for customers.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-check form-switch d-flex justify-content-center height-edit align-items-center">
                                        <input class="form-check-input switch-edit" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-end mx-4 mt-5">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>
        let role = document.getElementById("role");
        let role1 = document.getElementById("role1");
        let rolesub = document.getElementById("role-sub");
        let rolecancel = document.getElementById("rolecancel");

        let country = document.getElementById("country");
        let country1 = document.getElementById("country1");
        let countryadd = document.getElementById("countryadd");
        let countrycancel = document.getElementById("countrycancel");

        let state = document.getElementById("state");
        let state1 = document.getElementById("state1");
        let stateadd = document.getElementById("stateadd");
        let statecancel = document.getElementById("statecancel");

        let city = document.getElementById("city");
        let city1 = document.getElementById("city1");
        let cityadd = document.getElementById("cityadd");
        let citycancel = document.getElementById("citycancel");

        let cuisine = document.getElementById("cuisine");
        let cuisine1 = document.getElementById("cuisine1");
        let cuisineadd = document.getElementById("cuisineadd");
        let cuisinecancel = document.getElementById("cuisinecancel");

        rolesub.addEventListener("click", hiderole);
        rolecancel.addEventListener("click", showrole);
        function hiderole() {
            role.classList.add("d-none");
            role1.classList.remove("d-none");
        }
        function showrole() {
            role.classList.remove("d-none");
            role1.classList.add("d-none");
        }

        countryadd.addEventListener("click", hidecountry);
        countrycancel.addEventListener("click", showcountry);
        function hidecountry() {
            country.classList.add("d-none");
            country1.classList.remove("d-none");
        }
        function showcountry() {
            country.classList.remove("d-none");
            country1.classList.add("d-none");
        }

        stateadd.addEventListener("click", hidestate);
        statecancel.addEventListener("click", showstate);
        function hidestate() {
            state.classList.add("d-none");
            state1.classList.remove("d-none");
        }
        function showstate() {
            state.classList.remove("d-none");
            state1.classList.add("d-none");
        }

        cityadd.addEventListener("click", hidecity);
        citycancel.addEventListener("click", showcity);
        function hidecity() {
            city.classList.add("d-none");
            city1.classList.remove("d-none");
        }
        function showcity() {
            city.classList.remove("d-none");
            city1.classList.add("d-none");
        }

        cuisineadd.addEventListener("click", hidecuisine);
        cuisinecancel.addEventListener("click", showcuisine);
        function hidecuisine() {
            cuisine.classList.add("d-none");
            cuisine1.classList.remove("d-none");
        }
        function showcuisine() {
            cuisine.classList.remove("d-none");
            cuisine1.classList.add("d-none");
        }

    </script>
</body>