<?php 
?>
<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login.css">
    <div class="background-color d-flex align-items-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-10 col-lg-7">
                    <div class="card rounded-5 py-5">
                        <div class="card-body">
                            <h1 class="text-center fw-bold">Login</h1>
                            <h4 class="text-center">Welcome back</h4>
                            <?php echo validation_errors(); ?>
                            <form class="login-form" action="" method="post">
                                <div class="row flex-column align-items-center">
                                    <div class="col-11 col-sm-10 mb-3">
                                        <label for="email" class="form-label">Email</label>
                                        <input type="email" name="email" class="form-control" id="email" placeholder="Enter your email">
                                    </div>
                                    <div class="col-11 col-sm-10 mb-3">
                                        <label for="password" class="form-label">Password</label>
                                        <input type="password" name="Password" class="form-control" id="Password" placeholder="Enter your password">
                                    </div>
                                    <div class="col-6 mb-3 text-center">
                                        <input type="submit" name="submit" class="btn btn-primary px-4" value="Log in">
                                    </div>                
                                </div>
                            </form>
                            <p class="text-center">Don't have an account?<a href="<?php echo base_url();?>basedesign/signup"> Sign up</a></p>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
</body>
