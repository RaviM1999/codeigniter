    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.7/css/all.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/sidebar-navbar.css">
<body>
    <div class="custom-nav" id="custom-nav">
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
            <div class="container-fluid">
                <span id="move-sidebarin" class="move-sidebarin d-none">
                    <i class="fas fa-arrow-circle-right"></i>
                </span>
                <div class="nav-design">
                    <div>
                        <form>
                            <div class="position-relative">
                                <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                                <i><img src="<?php echo base_url();?>assets/images/search.png"></i>
                            </div>                  
                        </form>
                    </div>
                    <div class="d-flex column-gap-4 navbar-part">
                        <a href="#" role="button">
                            <img src="<?php echo base_url();?>assets/images/Notification 2.png">
                        </a>
                        <a href="#" role="button">
                            <img src="<?php echo base_url();?>assets/images/Message 37.png">
                        </a>
                        <div class="dropdown">
                            <button class="dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="<?php echo base_url();?>assets/images/logout.png">
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="<?php echo base_url(); ?>Basedesign/logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>              
            </div>
        </nav>
    </div>
    <div id="sidebar" class="side-bar-main">
        <div class="logo">
            <img src="<?php echo base_url();?>assets/images/logo.png" class="clogo">
            <i class="fas fa-window-close d-none" id="close-sidebar-x"></i>
        </div>
        <ul class="side-bar-list" id="side-bar-list">
            <a>
                <li><img src="<?php echo base_url();?>assets/images/dicon.png">Dashboard</li>
            </a>
            <a href="admin-merchant.php">
                <li><img src="<?php echo base_url();?>assets/images/merchant icon.png"> Merchant</li>
            </a>
            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                        <img src="<?php echo base_url();?>assets/images/merchant Items.png"> Merchant items <i class="fas fa-angle-down"></i>
                    </button>
                    <div id="flush-collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body">
                        <ul>
                            <li><a href="<?php echo base_url();?>merchant_items"><button>- Merchant Items</button></a></li>
                            <li><a href="<?php echo base_url();?>admin_merchant"><button>- Category</button></a></li>
                            <li><a href="sub-category.php"><button>- Sub Category</button></a></li>
                            <li><a href="variant.php"><button>- Variants</button></a></li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
            <a href="orders.php">
                <li><img src="<?php echo base_url();?>assets/images/ordersicon.png"> Order</li> 
            </a>
            <a href="customer.php">
                <li><img src="<?php echo base_url();?>assets/images/customersicon.png"> Customers</li>
            </a>
            <a href="driver.php">
                <li><img src="<?php echo base_url();?>assets/images/drivers.png"> Drivers</li>
            </a>
            <a>
                <li><img src="<?php echo base_url();?>assets/images/ratingsicon.png"> Ratings & Reviews</li>
            </a>
            <div class="accordion accordion-flush" id="accordionFlushExample1">
                <div class="accordion-item">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne1" aria-expanded="false" aria-controls="flush-collapseOne1">
                        <img src="<?php echo base_url();?>assets/images/Discount Shape 1.png" class=""> Offers <i class="fas fa-angle-down order-edit"></i>
                    </button>
                    <div id="flush-collapseOne1" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample1">
                    <div class="accordion-body">
                        <ul>
                            <li><a href="offers.php"><button>- Offers</button></a></li>
                            <li><a href="coupons.php"><button>- Coupons</button></a></li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
            <a>
                <li><img src="<?php echo base_url();?>assets/images/report.png"> Report</li> 
            </a>
            <a href="<?php echo base_url();?>setting">
                <li><img src="<?php echo base_url();?>assets/images/Setting.png">Setting</li>
            </a>
        </ul>
    </div>
    <script src="<?php echo base_url();?>assets/js/customjs.js"></script>
