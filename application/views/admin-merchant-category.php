<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/admin-merchant-category.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <p class="home-text">Home<span>/Merchant</span></p>
        <h3>Merchant category</h3>
        <div class="card">
            <div class="container tittle-box p-4">
                <div class="row">
                    <form class="tittle-box-form col-lg-10" role="search">
                        <div class="row">
                            <div class="col-6 col-md-2">
                                <input class="form-control title-form-edit" type="search" placeholder="Search" aria-label="Search">  
                            </div>
                            <div class="col-6 col-md-2">
                                <input class="form-control title-form-edit" type="search" placeholder="From Date" aria-label="Search">
                            </div>
                            <div class="col-6 col-md-2">
                                <input class="form-control title-form-edit" type="search" placeholder="To Date" aria-label="Search">
                            </div>
                            <div class="col-6 col-md-2">
                                <input class="form-control title-form-edit" type="search" placeholder="Status" aria-label="Search"> 
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn" name="cancel" id="cancel134"><i class="material-icons">&#xe5d5;</i></button>
                                <button type="button" class="btn" name="cancel" id="cancel165"><i class="material-icons">&#xe8b6;</i></button> 
                            </div>
                        </div>
                    </form>
                    <div class="col-lg-2 text-end">
                        <button type="button" class="btn" href="#" data-bs-target="#exampleModalToggle" data-bs-toggle="modal">
                            + Add
                        </button>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-borderless text-left">
                    <thead>
                        <th>ID</th>
                        <th>Merchant</th>
                        <th>Branch</th>
                        <th>Category</th>
                        <th class="text-center">Offer Percentage</th>
                        <th class="text-center">Offer Applied</th>
                        <th class="text-center">Created Date</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>KFC</td>
                            <td>Madurai</td>
                            <td><img src="<?php echo base_url();?>assets/images/noodles.png"> Chicken Noodles</td>
                            <td class="text-center">40%</td>
                            <td class="text-center">25%</td>
                            <td class="text-center">22/04/22</td>
                            <td class="text-center tbl-active">Active</td>
                            <td class="text-center">
                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <button>...</button>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#"><i class="material-icons text-center">&#xe417;</i>View</a></li>
                                    <li><a class="dropdown-item" href="#"><i class="material-icons text-center">&#xe254;</i>delete</a></li>
                                    <li><a class="dropdown-item" href="#"><i class="material-icons text-center">&#xe872;</i>Edit</a></li>
                                    </ul>
                            </div> 
                            </td> 
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="d-flex justify-content-end mx-4">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                        </li>
                     </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="customise-modal">
        <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <h1 class="title-1" id="title-1">Add Category</h1>
                        <form id="form-add-merchant-category" action="">
                            <div class="modal-form-edit row" id="modal-form-edit">
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="merchant">Product Name</label>
                                    <select class="form-select input-edit" name="merchant" id="merchant" aria-label="Default select example">
                                        <option selected>Merchant Name</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="branch">Branch</label>
                                    <input type="text" class="form-control input-edit" name="branch" id="branch"
                                        placeholder="Branch Name">
                                </div>
                                <div class="form-check check-bow-edit col-lg-3">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                                    <label class="form-check-label" for="flexCheckChecked">
                                        Checked checkbox
                                    </label>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="category">Category</label>
                                    <input type="text" class="form-control input-edit" name="category" id="category"
                                        placeholder="Category Name">
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="offerpercentage">Offer Percentage</label>
                                    <input type="text" class="form-control input-edit" name="offerpercentage" id="offerpercentage"
                                        placeholder="Offer Percentage">
                                </div>
                                <div class="col-lg-3">
                                    <label class="form-label d-none" for="status">Status</label>
                                    <select class="form-select input-edit" name="status" id="status" aria-label="Default select example">
                                        <option selected>Status</option>
                                        <option value="1">Active</option>
                                        <option value="2">Closed</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <label class="form-label d-none" for="description">Description</label>
                                    <textarea class="form-control input-edit" name="description" id="description" rows="3" placeholder="Description"></textarea>
                                </div>
                                <div class="col-lg-4">
                                    <label for="image" class="form-label d-none">Select Your Image</label>
                                    <input class="form-control input-edit" name="image" id="image" type="file">
                                </div>
                            </div>
                            <div class="modal-submit" id="modal-submit"> 
                                <div>
                                    <button type="button" class="btn" name="cancel" id="cancel">Cancel</button>
                                </div>
                                <div>
                                    <button type="button" class="btn" name="submit" id="submit1">Next
                                    </button>        
                                </div>
                            </div> 
                        </form>
                        <div class="total-view d-none" id="total-view">
                            <h1 class="">View Category Details</h1>
                            <div class="d-flex justify-content-end">
                                <button type="button" class="btn" name="edit" id="edit1">Edit</button>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 px-0">
                                        <img src="assets/images/burger.png">
                                    </div>
                                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 px-0">
                                        <h2>Chicken Burger</h2>
                                        <p>Merchant Name</p>
                                        <p>Branch Name</p>
                                        <p>Description</p>
                                    </div>
                                    <div class="col-6 col-sm-6 col-md-4 col-lg-2 sec2-list px-0">
                                        <p>Mr.Spencer Wise</p>
                                        <p>Chennai</p>
                                        <p>Crumbled tofu pressed together with lemongrass, sriracha</p>
                                    </div>
                                    <div class="col-6 col-sm-6 col-md-2 col-lg-2 sec3-list px-0">
                                        <p>Offer Percentage</p>
                                        <p>Offer Enabled</p>
                                        <p>Created Date</p>
                                        <p>Status</p>
                                    </div>
                                    <div class="col-6 col-sm-6 col-md-2 col-lg-2 sec4-list px-0">
                                        <p>20 %</p>
                                        <p><i class="material-icons text-center">&#xe5ca;</i></p>
                                        <p>22/04/23</p>
                                        <p class="tbl-active">Active</p>
                                    </div>
                                </div>
                                <div class="review-item d-none" id="review-item">
                                    <div>
                                        <button type="button" class="btn" name="cancel1" id="cancel1">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>

        $(document).ready(function(){
            $("#form-add-merchant-category").validate({
                rules: {
                    merchant: {
                        min: 1,
                    },
                    branch: {
                        required: true,
                    },
                    category: {
                        required: true,
                    },
                    offerpercentage: {
                        required: true,
                    },
                    status: {
                        min: 1,
                    },
                    description: {
                        required: true,
                    },
                    image: {
                        required: true,
                    },
                    
                },
                messages: {
                    merchant: {
                        min: "Select Your Merchant",
                    },
                    branch: {
                        required: "Enter Your Branch",
                    },
                    category: {
                        required: "Enter Category",
                    },
                    offerpercentage: {
                        required: "Enter Offer Percentage",
                    },
                    status: {
                        min: "Select Status",
                    },
                    description: {
                        required: "Enter Description",
                    },
                    image: {
                        required: "Upload Image",
                    },
                }
            });

            Submit = document.getElementById("submit1");
            Submit.addEventListener("click", secondForm);

            function secondForm() {
                if ($("#form-add-merchant-category").valid() == true) {
                    title1.classList.add("d-none");
                    modalForm.classList.add("d-none");
                    totalView.classList.remove("d-none");
                    reviewItem.classList.remove("d-none");
                    modalSubmit.classList.add("d-none");
                }
            }
            cancel1 = document.getElementById("cancel1");
            cancel1.addEventListener("click", firstForm);

            function firstForm() {
                title1.classList.remove("d-none");
                modalForm.classList.remove("d-none");
                totalView.classList.add("d-none");
                reviewItem.classList.add("d-none");
                modalSubmit.classList.remove("d-none");
            }
        });
    title1 = document.getElementById("title-1");
    totalView = document.getElementById("total-view");
    modalSubmit = document.getElementById("modal-submit");    
    modalForm = document.getElementById("modal-form-edit");   
    reviewItem = document.getElementById("review-item");
    </script>
</body>
