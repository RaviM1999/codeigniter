<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/add-merchant-item.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3>Add Products</h3>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col">
                        <div class="title-show">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3 col-xl-3 px-0">
                                        <p class="title-1" id="title-1"><span id="no1">01</span>Product Details</p>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-xl-2 px-0 dot1-step">
                                        <p class="stp1" id="stp1">- - - - - - - - - - - - </p>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-xl-3 px-0">
                                        <p class="title-2" id="title-2"><span id="no2">02</span>Variant Details</p>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-xl-2 px-0 dot2-step">
                                        <p class="stp2" id="stp2">- - - - - - - - - - - - </p>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-xl-2 px-0">
                                        <p class="title-3" id="title-3"><span id="no3">03</span>View</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="product/add_product" id="form-add-merchant-item" enctype="multipart/form-data" method="post">
                    <div id="form-contents">
                        <div class="container form-contents">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="merchant_name">Merchant Name</label>
                                            <select class="form-select input-edit" name="Product_name" id="merchant_name" aria-label="Default select example">
                                                <option disabled selected>Merchant Name</option>
                                                <option value="kfc">KFC</option>
                                                <option value="Mac Donalds">Mac Donalds</option>
                                                <option value="Chill Out">Chill Out</option>
                                                <option value="Jr.Kuppanna">Jr.Kuppanna</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="branch">Branch</label>
                                            <select class="form-select input-edit" name="Product_branch" id="branch" aria-label="Default select example">
                                                <option disabled selected>Branch</option>
                                                <option value="Madurai">Madurai</option>
                                                <option value="Chennai">Chennai</option>
                                                <option value="Salem">Salem</option>
                                                <option value="Erode">Erode</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="category">Category</label>
                                            <select class="form-select input-edit" name="Product_category" id="category" aria-label="Default select example">
                                                <option disabled selected>Category</option>
                                                <option value="Burger">Burger</option>
                                                <option value="Biryani">Biryani</option>
                                                <option value="Sandwitch">Sandwitch</option>
                                                <option value="Noodles">Noodles</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="sub_category">Sub Category</label>
                                            <select class="form-select input-edit" name="Product_sub_category" id="sub_category" aria-label="Default select example">
                                                <option disabled selected>Sub Category</option>
                                                <option value="Chicken">Chicken</option>
                                                <option value="Veg">Veg</option>
                                                <option value="Mushroom">Mushroom</option>
                                                <option value="Egg">Egg</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="price">Price</label>
                                            <input type="text" class="form-control input-edit numbersOnly" name="Product_price" id="price"
                                                placeholder="Price">
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="product">Product</label>
                                            <input type="text" class="form-control input-edit" name="Product_nam" id="product"
                                                placeholder="Product">
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="description">Description</label>
                                            <textarea class="form-control input-edit" name="Product_description" id="description" rows="3" placeholder="Description"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div>
                                        <label class="form-label d-none" for="status">Status</label>
                                        <select class="form-select input-edit" name="Product_status" id="status" aria-label="Default select example">
                                            <option disabled selected>Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label for="image" class="form-label d-none">Select Your Image</label>
                                        <input class="form-control input-edit" name="Product_image" id="image" type="file">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="next-form" id="next-form">
                            <div>
                                <a href="<?php echo base_url();?>merchant_items">
                                    <button type="button" class="btn" name="cancel" id="cancel">Cancel</button>
                                </a>
                            </div>
                            <div>
                                <button type="button" class="btn submit1" name="submit1" id="submit1" value="Next">Next</button>      
                            </div>
                        </div>
                    </div>
                    <div class="d-none" id="form-contents1">
                        <div id="variant_form">
                            <div class="container form-contents1 p-0 mx-0" >
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="form-label d-none" for="variant">Variant Name</label>
                                                <select class="form-select input-edit" name="Product_Var_variant" id="variant" aria-label="Default select example">
                                                    <option disabled selected>Variant Name</option>
                                                    <option value="Regular Size">Regular Size</option>
                                                    <option value="Medium Size">Medium Size</option>
                                                    <option value="Large Size">Large Size</option>
                                                </select>
                                            </div>
                                            <div  class="col-lg-6">
                                                <label class="form-label d-none" for="price1">Price</label>
                                                <input type="text" class="form-control input-edit numbersOnly" name="Product_Var_price" id="price1"
                                                    placeholder="Price">
                                            </div>
                                            <div  class="col-lg-6">
                                                <label class="form-label d-none" for="description1">Description</label>
                                                <textarea class="form-control input-edit" name="Product_Var_description" id="description1" rows="3" placeholder="Description"></textarea>
                                            </div>
                                            <div  class="col-lg-6">
                                                <label class="form-label d-none" for="status1">1</label>
                                                <select class="form-select input-edit" name="Product_Var_status" id="status1" aria-label="Default select example">
                                                    <option disabled selected>Status</option>
                                                    <option value="Active">Active</option>
                                                    <option value="Inactive">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <label for="image1" class="form-label d-none">Select Your Image</label>
                                                <input class="form-control input-edit" name="Product_Var_image" id="image1" type="file">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="add_input"></div>
                        </div>
                        <div class="next-form1">
                            <div>
                                <button type="button" class="btn" name="cancel" id="cancel1">Back</button>
                            </div>
                            <div>
                                <button type="submit" class="btn submit1" id="submit2" name="submit2">Next</button>        
                            </div>
                            <div>
                                <button type="button" class="btn add-variant" id="add_variant" name="add_variant">+ Add</button>        
                            </div>
                        </div> 
                    </div>
                </form>
                <div class="preview-products d-none" id="preview-products">
                    <div class="container">
                        <div class="row ms-1">
                            <div class="col-lg-3">
                                <img src="<?php echo base_url();?>assets/images/burger.png" class="img-fluid">
                            </div>
                            <div class="col">
                                <div class="row">
                                    <h3>Chicken Burger</h3>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <table class="table table-borderless mb-0">
                                            <tr>
                                                <td class="py-1">Price</td>
                                                <td class="py-1">400</td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Merchant Name</td>
                                                <td class="py-1">KFC</td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Branch</td>
                                                <td class="py-1">Chennai</td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Category</td>
                                                <td class="py-1">Burger</td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Sub-Category</td>
                                                <td class="py-1">Chicken-Burger</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td class="col-6 py-1">Description</td>
                                                <td class="col-6 py-1">Crumbled pressed together with lemongrass</td>
                                            </tr>
                                            <tr>
                                                <td class="col-6 py-1">Status</td>
                                                <td class="col-6 py-1">Active</td>
                                            </tr>
                                        </table>
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                        <div class="row m-3">
                            <ul class="nav nav-underline" id="myTab" role="tablist">
                                <li class="nav-item col-12 col-sm-3 col-lg-2" role="presentation">
                                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Regular Size</button>
                                </li>
                                <li class="nav-item col-12 col-sm-3 col-lg-2" role="presentation">
                                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Medium Size</button>
                                </li>
                                <li class="nav-item col-12 col-sm-3 col-lg-2" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact-tab-pane" type="button" role="tab" aria-controls="contact-tab-pane" aria-selected="false">Large Size</button>
                                </li>
                            </ul>
                            <hr class="tabs-line">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                                    <div class="row mt-4">
                                        <div class="col-12 col-lg-5">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td class="col-6 py-1">Price</td>
                                                    <td class="py-1">500</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Description</td>
                                                    <td class="py-1">Crumbled pressed together with lemongrass</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Created Date</td>
                                                    <td class="col-6 py-1">22/04/23</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Status</td>
                                                    <td class="col-6 py-1">Active</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-3 col-lg-3">
                                            <div id="carouselExample" class="carousel slide">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                    <img src="<?php echo base_url();?>assets/images/chickenburger.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>assets/images/chickenburger.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>assets/images/chickenburger.png" class="d-block w-100" alt="...">
                                                    </div>
                                                </div>
                                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Previous</span>
                                                </button>
                                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Next</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                                    <div class="row mt-4">
                                        <div class="col-12 col-lg-5">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td class="col-6 py-1">Price</td>
                                                    <td class="py-1">500</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Description</td>
                                                    <td class="py-1">Crumbled pressed together with lemongrass</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Created Date</td>
                                                    <td class="col-6 py-1">22/04/23</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Status</td>
                                                    <td class="col-6 py-1">Active</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-3 col-lg-3">
                                            <div id="carouselExample2" class="carousel slide">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                    <img src="<?php echo base_url();?>assets/images/chickenburger.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>assets/images/chickenburger.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>assets/images/chickenburger.png" class="d-block w-100" alt="...">
                                                    </div>
                                                </div>
                                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample2" data-bs-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Previous</span>
                                                </button>
                                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample2" data-bs-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Next</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact-tab-pane" role="tabpanel" aria-labelledby="contact-tab" tabindex="0">
                                    <div class="row mt-4">
                                        <div class="col-12 col-lg-5">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td class="col-6 py-1">Price</td>
                                                    <td class="py-1">500</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Description</td>
                                                    <td class="py-1">Crumbled pressed together with lemongrass</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Created Date</td>
                                                    <td class="col-6 py-1">22/04/23</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Status</td>
                                                    <td class="col-6 py-1">Active</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-3 col-lg-3">
                                            <div id="carouselExample3" class="carousel slide">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                    <img src="<?php echo base_url();?>assets/images/chickenburger.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>assets/images/chickenburger.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>assets/images/chickenburger.png" class="d-block w-100" alt="...">
                                                    </div>
                                                </div>
                                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample3" data-bs-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Previous</span>
                                                </button>
                                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample3" data-bs-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Next</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="final-view" id="final-view">
                            <div>
                                <button type="button" class="btn" name="cancel" id="cancel2">Back</button>
                            </div>
                            <div>
                                <button type="button" class="btn submit3" name="submit3" id="submit3">Save</button>        
                            </div>
                        </div> 
                    </div>
                </div>               
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>
        $(document).ready(function(){
            // $("#form-add-merchant-item").validate({
            //     rules: {
            //         Product_name: {
            //             required: true,
            //         },
            //         Product_branch: {
            //             required: true,
            //         },
            //         Product_status: {
            //             required: true,  
            //         },
            //         Product_category: {
            //             required: true,  
            //         },
            //         Product_sub_category: {
            //             required: true,
            //         },
            //         Product_image: {
            //             required: true,
            //         },
            //         Product_price: {
            //             required: true,
            //             min: 1,
            //             number: true,
            //         },
            //         Product_nam: {
            //             required: true,
            //             minlength: 5,  
            //         },
            //         Product_description: {
            //             required: true,
            //             minlength: 6,  
            //         },
            //         Product_Var_variant: {
            //             required: true,
            //         },
            //         Product_Var_price: {
            //             required: true,
            //             min: 1,
            //             number: true,
            //         },
            //         Product_Var_image: {
            //             required: true,
            //         },
            //         Product_Var_status: {
            //             required: true,
            //         },
            //         Product_Var_description: {
            //             required: true,
            //             minlength: 6,
            //         },
            //     },
            //     messages: {
            //         Product_name: {
            //             required: "Select Merchant Name",
            //         },
            //         Product_branch: {
            //             required: "Select Branch",
            //         },
            //         Product_status: {
            //             required: "Enter Status",
            //         },
            //         Product_category: {
            //             required: "Select Category",
            //         },
            //         Product_sub_category: {
            //             required: "Select Sub Category",
            //         },
            //         Product_image: {
            //             required: "Upload Image",
            //         },
            //         Product_price: {
            //             required: "Enter Price",
            //         },
            //         Product_nam: {
            //             required: "Enter Product",
            //         },
            //         Product_description: {
            //             required: "Enter Description",
            //         },
            //         Product_Var_variant: {
            //             min: "Enter Variant",
            //         },
            //         price1: {
            //             required: "Enter Price",
            //         },
            //         Product_Var_image: {
            //             required: "Upload Image",
            //         },
            //         Product_Var_status: {
            //             min: "Select Status",
            //         },
            //         Product_Var_description: {
            //             required: "Enter Description",
            //         },
            //     }
            // });
            jQuery('.numbersOnly').keyup(function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });
            let form1 = document.getElementById("submit1");
            form1.addEventListener("click", firstForm);

            function firstForm(){
                if ($("#form-add-merchant-item").valid() == true) {
                    formContents.classList.add("d-none");
                    form1.classList.add("d-none");
                    title2.classList.add("add-title2");
                    stp2.classList.add("add-stp2");
                    title1.classList.add("remove-title1");
                    stp1.classList.add("remove-stp1");
                    no1.classList.add("remove-no1");
                    no2.classList.add("remove-no2");
                    formContents1.classList.remove("d-none")
                }
            }
            
            let cancleForm1 = document.getElementById("cancel1");
            let secondForm = document.getElementById("submit2");
            cancleForm1.addEventListener("click", movefirstForm);
            secondForm.addEventListener("click", thirdForm);
            
            function movefirstForm() {
                formContents.classList.remove("d-none");
                form1.classList.remove("d-none");
                formContents1.classList.add("d-none")
                title2.classList.remove("add-title2");
                stp2.classList.remove("add-stp2");
                title1.classList.remove("remove-title1");
                stp1.classList.remove("remove-stp1");
                no1.classList.remove("remove-no1");
                no2.classList.remove("remove-no2");
            }

            function thirdForm(){
                if ($("#form-add-merchant-item").valid() == true) {
                    formContents1.classList.add("d-none");
                    previewProducts.classList.remove("d-none");
                    title2.classList.remove("add-title2");
                    stp2.classList.remove("add-stp2");
                    no2.classList.remove("remove-no2");
                    title3.classList.add("add-no3");
                    no3.classList.add("add-3");
                }
            }
            
            let cancleForm2 = document.getElementById("cancel2");
            let finalSubmit = document.getElementById("submit3");
            cancleForm2.addEventListener("click", backForm);
            finalSubmit.addEventListener("click", finalform)

            function backForm() {
                previewProducts.classList.add("d-none");
                formContents1.classList.remove("d-none");
                title2.classList.add("add-title2");
                stp2.classList.add("add-stp2");
                no2.classList.add("remove-no2");
                title3.classList.remove("add-no3");
                no3.classList.remove("add-3");
            }

            function finalform() {
                if ($("#form-add-merchant-item").valid() == true) {
                    $("#form-add-merchant-item").submit();
                }
            }
        });
        
        let formContents =document.getElementById("form-contents");
        let formContents1 = document.getElementById("form-contents1");
        let previewProducts = document.getElementById("preview-products");
        let title1 = document.getElementById("title-1");
        let stp1 = document.getElementById("stp1");
        let title2 = document.getElementById("title-2");
        let stp2 = document.getElementById("stp2");
        let title3 = document.getElementById("title-3");
        let no1 = document.getElementById("no1");
        let no2 = document.getElementById("no2");
        let no3 = document.getElementById("no3");

        let control = document.getElementById("variant_form").innerHTML;
        // console.log(control);
        document.getElementById("add_variant").addEventListener("click", addfunction);
        function addfunction() {
            document.getElementById("add_input").innerHTML += control;
        }
    </script>
</body>
