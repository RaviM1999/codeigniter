<body class="background-color">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/signup.css">
    <div class="container mt-5">
        <h1 class="text-center">Sign up</h1>
        <?php echo validation_errors(); ?>
        <form action="" id="sign_up" method="post">
            <div class="row g-3 mt-2 justify-content-center">
                <div class="col-12 col-sm-4 d-none">
                    <label for="firstname" class="col-sm-2 col-form-label text-nowrap">First name</label>
                  <input type="text" class="form-control" name="firstname" id="firstname" placeholder="First name" aria-label="First name">
                </div>
                <div class="col-12 col-sm-4 d-none">
                    <label for="lastname" class="col-sm-2 col-form-label text-nowrap">Last name</label>
                  <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last name" aria-label="Last name">
                </div>
                <div class="col-12 col-sm-8">
                    <label for="fullname" class="col-sm-2 col-form-label text-nowrap">Full name</label>
                  <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Full name" aria-label="Full name">
                </div>
                <div class="col-12 col-sm-8">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                </div>
                <div class="col-12 col-sm-8">
                    <label for="contactnumber" class="form-label">Contact Number</label>
                    <input type="text" class="form-control" name="contactnumber" id="contactnumber" placeholder="Contact Number">
                </div>
                <div class="col-12 col-sm-8">
                    <label for="address" class="form-label">Address</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                </div>
                <div class="col-12 col-sm-8">
                    <label for="Password" class="form-label">Password</label>
                    <input type="password" class="form-control" name="Password" id="Password" placeholder="Password">
                </div>
            </div>
            <div>
                <div class="text-center mt-3">
                    <input class="btn btn-primary rounded-pill ps-4 pe-4" id="formsubmit" name="submit" type="submit"  value="Sign Up">
                </div>
                <div class="text-center mt-3">
                    <p>Already have an account?<a href="<?php echo base_url();?>Basedesign/index">Login here</a></p>
                </div>
                <div class="text-center mt-3">
                    <p>Copyright © 2024 Ayro UI. All Rights Reserved</p>
                </div>
            </div>
        </form>
        <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    </div>