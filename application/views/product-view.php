<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/add-merchant-item.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3>View Product Details</h3>
                </div>
            </div>
                <div class="preview-products" id="preview-products">
                    <div class="container">
                        <div class="row ms-1">
                            <div class="col-lg-3">
                                <!-- <img src="<?php echo base_url();?>assets/images/burger.png" class="img-fluid"> -->
                                <img src="<?php echo base_url();?>uploads/<?php echo $details["Product_image"]?>" class="img-fluid">
                            </div>
                            <div class="col">
                                <div class="row">
                                    <h3><?php echo $details["Product_category"]?></h3>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <table class="table table-borderless mb-0">
                                            <tr>
                                                <td class="py-1">Price</td>
                                                <td class="py-1"><?php echo $details["Product_price"]?></td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Merchant Name</td>
                                                <td class="py-1"><?php echo $details["Product_name"]?></td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Branch</td>
                                                <td class="py-1"><?php echo $details["Product_branch"]?></td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Category</td>
                                                <td class="py-1"><?php echo $details["Product_category"]?></td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Sub-Category</td>
                                                <td class="py-1"><?php echo $details["Product_sub_category"]?></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td class="col-6 py-1">Description</td>
                                                <td class="col-6 py-1"><?php echo $details["Product_description"]?></td>
                                            </tr>
                                            <tr>
                                                <td class="col-6 py-1">Status</td>
                                                <td class="col-6 py-1"><?php echo $details["Product_status"]?></td>
                                            </tr>
                                        </table>
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                        <div class="row m-3">
                            <ul class="nav nav-underline" id="myTab" role="tablist">
                                <li class="nav-item col-12 col-sm-3 col-lg-2" role="presentation">
                                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Regular Size</button>
                                </li>
                                <li class="nav-item col-12 col-sm-3 col-lg-2" role="presentation">
                                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Medium Size</button>
                                </li>
                                <li class="nav-item col-12 col-sm-3 col-lg-2" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact-tab-pane" type="button" role="tab" aria-controls="contact-tab-pane" aria-selected="false">Large Size</button>
                                </li>
                            </ul>
                            <hr class="tabs-line">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                                    <div class="row mt-4">
                                        <div class="col-12 col-lg-5">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td class="col-6 py-1">Price</td>
                                                    <td class="py-1"><?php echo $details["Product_Var_price"]?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Description</td>
                                                    <td class="py-1"><?php echo $details["Product_Var_description"]?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Created Date</td>
                                                    <td class="col-6 py-1">22/04/23</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Status</td>
                                                    <td class="col-6 py-1"><?php echo $details["Product_Var_status"]?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-3 col-lg-3">
                                            <div id="carouselExample" class="carousel slide">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                    <img src="<?php echo base_url();?>uploads/<?php echo $details["Product_Var_image"]?>" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>uploads/<?php echo $details["Product_Var_image"]?>" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>uploads/<?php echo $details["Product_Var_image"]?>" class="d-block w-100" alt="...">
                                                    </div>
                                                </div>
                                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Previous</span>
                                                </button>
                                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Next</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                                    <div class="row mt-4">
                                        <div class="col-12 col-lg-5">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td class="col-6 py-1">Price</td>
                                                    <td class="py-1"><?php echo $details["Product_Var_price"]?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Description</td>
                                                    <td class="py-1"><?php echo $details["Product_Var_description"]?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Created Date</td>
                                                    <td class="col-6 py-1">22/04/23</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Status</td>
                                                    <td class="col-6 py-1"><?php echo $details["Product_Var_status"]?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-3 col-lg-3">
                                            <div id="carouselExample2" class="carousel slide">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                    <img src="<?php echo base_url();?>uploads/<?php echo $details["Product_Var_image"]?>" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>uploads/<?php echo $details["Product_Var_image"]?>" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>uploads/<?php echo $details["Product_Var_image"]?>" class="d-block w-100" alt="...">
                                                    </div>
                                                </div>
                                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample2" data-bs-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Previous</span>
                                                </button>
                                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample2" data-bs-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Next</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact-tab-pane" role="tabpanel" aria-labelledby="contact-tab" tabindex="0">
                                    <div class="row mt-4">
                                        <div class="col-12 col-lg-5">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td class="col-6 py-1">Price</td>
                                                    <td class="py-1"><?php echo $details["Product_Var_price"]?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Description</td>
                                                    <td class="py-1"><?php echo $details["Product_Var_description"]?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Created Date</td>
                                                    <td class="col-6 py-1">22/04/23</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Status</td>
                                                    <td class="col-6 py-1"><?php echo $details["Product_Var_status"]?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-3 col-lg-3">
                                            <div id="carouselExample3" class="carousel slide">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                    <img src="<?php echo base_url();?>uploads/<?php echo $details["Product_Var_image"]?>" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>uploads/<?php echo $details["Product_Var_image"]?>" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img src="<?php echo base_url();?>uploads/<?php echo $details["Product_Var_image"]?>" class="d-block w-100" alt="...">
                                                    </div>
                                                </div>
                                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample3" data-bs-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Previous</span>
                                                </button>
                                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample3" data-bs-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="visually-hidden">Next</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="final-view" id="final-view">
                            <div>
                                <button type="button" class="btn d-none" name="cancel" id="cancel2">Back</button>
                            </div>
                            <div>
                                <a href="<?php echo base_url();?>merchant_items">
                                <button type="button" class="btn submit3" name="submit3" id="submit3">Save</button></a>        
                            </div>
                        </div> 
                    </div>
                </div>               
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <!-- <script>
        $(document).ready(function() {
            let form1 = document.getElementById("submit1");
            form1.addEventListener("click", firstForm);

            function firstForm(){
                if ($("#form-add-merchant-item").valid() == true) {
                    formContents.classList.add("d-none");
                    form1.classList.add("d-none");
                    title2.classList.add("add-title2");
                    stp2.classList.add("add-stp2");
                    title1.classList.add("remove-title1");
                    stp1.classList.add("remove-stp1");
                    no1.classList.add("remove-no1");
                    no2.classList.add("remove-no2");
                    formContents1.classList.remove("d-none")
                }
            }
            
            let cancleForm1 = document.getElementById("cancel1");
            let secondForm = document.getElementById("submit2");
            cancleForm1.addEventListener("click", movefirstForm);
            secondForm.addEventListener("click", thirdForm);
            
            function movefirstForm() {
                formContents.classList.remove("d-none");
                form1.classList.remove("d-none");
                formContents1.classList.add("d-none")
                title2.classList.remove("add-title2");
                stp2.classList.remove("add-stp2");
                title1.classList.remove("remove-title1");
                stp1.classList.remove("remove-stp1");
                no1.classList.remove("remove-no1");
                no2.classList.remove("remove-no2");
            }

            function thirdForm(){
                if ($("#form-add-merchant-item").valid() == true) {
                    formContents1.classList.add("d-none");
                    previewProducts.classList.remove("d-none");
                    title2.classList.remove("add-title2");
                    stp2.classList.remove("add-stp2");
                    no2.classList.remove("remove-no2");
                    title3.classList.add("add-no3");
                    no3.classList.add("add-3");
                }
            }
            
            let cancleForm2 = document.getElementById("cancel2");
            let finalSubmit = document.getElementById("submit3");
            cancleForm2.addEventListener("click", backForm);
            finalSubmit.addEventListener("click", finalform)

            function backForm() {
                previewProducts.classList.add("d-none");
                formContents1.classList.remove("d-none");
                title2.classList.add("add-title2");
                stp2.classList.add("add-stp2");
                no2.classList.add("remove-no2");
                title3.classList.remove("add-no3");
                no3.classList.remove("add-3");
            }

            function finalform() {
                if ($("#form-add-merchant-item").valid() == true) {
                    $("#form-add-merchant-item").submit();
                }
            }
        });
        
        let formContents =document.getElementById("form-contents");
        let formContents1 = document.getElementById("form-contents1");
        let previewProducts = document.getElementById("preview-products");
        let title1 = document.getElementById("title-1");
        let stp1 = document.getElementById("stp1");
        let title2 = document.getElementById("title-2");
        let stp2 = document.getElementById("stp2");
        let title3 = document.getElementById("title-3");
        let no1 = document.getElementById("no1");
        let no2 = document.getElementById("no2");
        let no3 = document.getElementById("no3");
    </script> -->
</body>
