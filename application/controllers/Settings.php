<?php 
class Settings extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
        $this->load->model('Settings_model');
        $this->load->model('Login_model');
        $this->load->library('session');
        $this->load->library('form_validation');
    }

    public function setting_page() {
        // print_r ($this->session->userdata());
        // exit;
        if($this->session->userdata() == FALSE) {
            redirect ('login');
        } else {
            $user = array('email'=>$this->session->userdata('email'));
            $profiledetails['profile'] = $this->Settings_model->profile($user);
            $profiledetails['infos']=$this->Settings_model->countrydata();
            $profiledetails['states']=$this->Settings_model->statedata();
            $profiledetails['citys']=$this->Settings_model->citydata();
            $profiledetails['cuisine']=$this->Settings_model->cuisinedata();
            //  echo "<pre>";
            //  print_r ($profiledetails);
            //  exit;
            $this->load->view('setting', $profiledetails);
        }
    }
    public function country_type() {
        if($this->input->post('countrysubmit')) {
            $country=array(
                'country_name'=>$this->input->post('country'),
                'status'=>$this->input->post('status')
                );
                $this->Settings_model->countrys($country);
                redirect ('setting');
        }
    }
    public function state_type() {
        if($this->input->post('statesubmit')) {
            $state=array(
                'con_name'=>$this->input->post('countryname'),
                'state_name'=>$this->input->post('statename'),
                'state_status'=>$this->input->post('statestatus')
            );
            // echo "<pre>";
            // print_r ($state);
            // exit;
            $this->Settings_model->state($state);
            redirect ('setting');
        }
    }  
    public function city_type() {
        if($this->input->post('citysubmit')) {
            $city=array(
                'city_country'=>$this->input->post("citycountry"),
                'city_state'=>$this->input->post("citystate"),
                'city_name'=>$this->input->post("cityname"),
                'city_status'=>$this->input->post("citystatus")
            );
            $this->Settings_model->city($city);
            redirect ('setting');
        }
    }
    public function cuisine_type() {
        if($this->input->post('cuisinesubmit')) {
            $cuisine=array(
                'cuisine_name'=>$this->input->post("cuisinename"),
                'cuisine_status'=>$this->input->post("cuisinestatus")
            );
            // echo "<pre>";
            // print_r ($cuisine);
            // exit;
            $this->Settings_model->cuisine($cuisine);
            redirect ('setting');
        }
    }
}