<?php 
class Adminmerchant extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
        $this->load->model('Login_model');
        $this->load->library('session');
        $this->load->library('form_validation');
    }
    
    public function adminmerchant() {
        $this->load->view('admin-merchant-category');
    }
}