<?php 
class Product extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Product_model');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->helper(array('form','url'));
    }
    public function add_product() { 
        if($this->input->post()) {
            
            // echo "<pre>";
            // print_r($_FILES);
            // exit;

            foreach($_FILES as $x => $file) {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['file_name']            = date("Ymdhi").$file['name'];
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload($x)) {
                    // echo "success---1";
                } else {
                    echo "Fail";
                }
            }
            $product = $this->input->post();  
            $product['Product_image'] = date("Ymdhi").$_FILES['Product_image']['name'];
            $product['Product_Var_image'] =  date("Ymdhi").$_FILES['Product_Var_image']['name'];
            unset($product['submit2']);
                echo "<pre>";
                print_r($product);
                exit;
            // $this->Product_model->add_products($product);
            $currentproduct['details'] = $product;
            $this->view_product($currentproduct);
        } else {
            $this->load->view('add-merchant-item');
        } 
    }
    public function view_product($currentproduct) {
        $this->load->view('product-view', $currentproduct);
    }
}
