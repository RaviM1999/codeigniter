<?php 
class Merchantitem extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
        $this->load->model('Merchantitem_model');
    }
    
    public function merchant_item() {
        $merchantlist['productlist'] = $this->Merchantitem_model->productdata();
        // echo "<pre>";
        // print_r($merchantlist['productlist']);
        // exit;
        $this->load->view('merchant-item', $merchantlist);
    }
}