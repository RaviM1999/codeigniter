<?php 
class Settings_model extends CI_Model {
    public function profile($user) {
        $query = $this->db->get_where('login',$user);
        return $query->result_array();  
    }
    public function countrys($country) {
        $this->db->insert('country',$country);
    }
    public function countrydata() {
       $con_data = $this->db->get('country');
       return $con_data->result();
    }
    public function state($state) {
        $this->db->insert('state',$state);
    }
    public function statedata() {
        $state_data = $this->db->get('state');
        return $state_data->result();
    }
    public function city($city) {
        $this->db->insert('city',$city);
    }
    public function citydata() {
        $city_data = $this->db->get('city');
        return $city_data->result();
    }
    public function cuisine($cuisine) {
        $this->db->insert('cuisine_list',$cuisine);
    }
    public function cuisinedata() {
        $cuisine_data = $this->db->get('cuisine_list');
        return $cuisine_data->result();
    }
}