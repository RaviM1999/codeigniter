function togglecount() {
    var isChecked = document.getElementById("togglebutton").checked;
    console.log(isChecked)
    let value = " ";   
        if(isChecked==true)  {
             value = confirm("Switch to Bill Annually");
             console.log('------------------',value);
             if(value==true){
                document.getElementById("togglebutton").checked = true;
            } else if(value==false) {
                document.getElementById("togglebutton").checked = false;
            } 
        } else {
            value = confirm("switch to Bill monthly");
            console.log('+++++++++++++++',value);
                if(value==true){
                    document.getElementById("togglebutton").checked = false;
                } else if(value==false) {
                    document.getElementById("togglebutton").checked = true;
                } 
            }          
}
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    // navText : ['<img src="../images/button-prev.png">','<img src="../images/button-next.png">'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
$(document).ready(function() {
    $("#myforms").validate({
        rules: {
            fullname: {
                required: true,
            },
            email: {
                required: true,
            },
            phonenumber: {
                required: true,
            },
            subject: {
                required: true,
            },
            message: {
                required: true,
            } ,  
        },
        messages: {
            fullname: {
                required: "Please enter a full name.",
            },
            email: {
                required:  "Please enter a valid Last Name.",
            },
            phonenumber: {
                required: "Please enter phone number",
            },
            subject: {
                required: "Please enter a subject",
            },
            message: {
                required: "Please enter a message",
            } 
        }
    });
    $("#btnsubmit").click(function(){
        $("#myforms").valid();
      });

});
// -----------------------------------------signup---------------------------------------
$(document).ready(function() {
    $("#sign_up").validate({
        rules: {
            Firstname: {
                required: true,
            },
            Lastname: {
                required: true,
            },
            email: {
                required: true,
            },
            password: {
                required: true,
            },
            confirmpassword: {
                required: true,
            },
            checkbox: {
                required: true,
            } ,  
        },
        messages: {
            Firstname: {
                required: "Please enter a first name.",
            },
            Lastname: {
                required:  "Please enter a valid Last Name.",
            },
            email: {
                required: "Please enter email",
            },
            password: {
                required: "Please enter a password",
            },
            confirmpassword: {
                required: "Please enter a confirm password",
            },
            checkbox: {
                required: "Please click the checkbox",
            }  
        }
    });
    $("#formsubmit").click(function(){
        $("#sign_up").valid();
      });

});